FROM openjdk:17-jdk-slim

# Mettre à jour les paquets et installer Maven et Tomcat
RUN apt-get update && \
    apt-get install -y maven git curl wget && \
    rm -rf /var/lib/apt/lists/*

# Installer Tomcat
RUN wget https://downloads.apache.org/tomcat/tomcat-10/v10.1.24/bin/apache-tomcat-10.1.24-deployer.tar.gz && \
    tar xzvf apache-tomcat-10.1.24-deployer.tar.gz -C /opt && \
    rm apache-tomcat-10.1.24-deployer.tar.gz

# Définir les variables d'environnement pour Tomcat
ENV CATALINA_HOME /opt/apache-tomcat-10.1.24-deployer
ENV PATH $CATALINA_HOME/bin:$PATH

# Dossier de travail
WORKDIR /workspace

# Monter le dossier projet local dans /workspace
VOLUME /workspace

# Commande par défaut
CMD ["tail", "-f", "/dev/null"]
